from django.db import models


class Country(models.Model):
    image = models.ImageField(upload_to='media/', null=True, blank=True)
    name = models.CharField(max_length=256)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Party(models.Model):
    image = models.ImageField(upload_to='media/', null=True, blank=True)
    name = models.CharField(max_length=256)
    country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True, related_name='party_set')
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Candidate(models.Model):
    image = models.ImageField(upload_to='media/', null=True, blank=True)
    name = models.CharField(max_length=256)
    country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True, related_name='candidate_set')
    party = models.ForeignKey(Party, on_delete=models.CASCADE, null=True, related_name='candidate_set')
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name
