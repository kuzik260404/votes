from django.contrib import admin
from .models import Candidate, Party, Country


class CandidateAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'related_party_name', 'related_country_name', 'description')

    @staticmethod
    def related_country_name(obj):
        return obj.country.name if obj.country else "No Country"

    @staticmethod
    def related_party_name(obj):
        return obj.party.name if obj.party else "No Party"


class PartyAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'related_country_name', 'description')

    @staticmethod
    def related_country_name(obj):
        return obj.country.name if obj.country else "No Country"


class CountryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description')


admin.site.register(Candidate, CandidateAdmin)
admin.site.register(Party, PartyAdmin)
admin.site.register(Country, CountryAdmin)
